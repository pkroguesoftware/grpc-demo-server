package kz.grps.demo.generated.goodbyeworld;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class GoodByeRequestTest {

    @Test
    public void createRequest() {
        String firstName = "Avetik";
        String lastName = "Eranosyan";
        GoodByeRequest request = GoodByeRequest.newBuilder().setFirstName(firstName).setLastName(lastName).build();
        assertNotNull(request);
        assertEquals(firstName, request.getFirstName(), "First name is not equal");
        assertEquals(lastName, request.getLastName(), "Last name is not equal");
    }
}