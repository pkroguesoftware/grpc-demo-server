package kz.grps.demo.server;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import kz.grps.demo.service.HelloService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
// TODO: 09.04.2021 Get port from config
public class ServerRunner implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        Server server = ServerBuilder
                .forPort(8080)
                .addService(new HelloService()).build();

        server.start();
        server.awaitTermination();
    }
}
