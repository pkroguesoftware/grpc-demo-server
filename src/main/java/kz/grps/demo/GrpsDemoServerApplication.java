package kz.grps.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrpsDemoServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GrpsDemoServerApplication.class, args);
	}

}
