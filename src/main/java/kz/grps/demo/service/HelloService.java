package kz.grps.demo.service;

import grpc.hello.HelloGrpc;
import hello.HelloReply;
import hello.HelloRequest;
import io.grpc.stub.StreamObserver;
import org.springframework.stereotype.Service;

@Service
public class HelloService extends HelloGrpc.HelloImplBase {
    @Override
    public void hello(HelloRequest request, StreamObserver<HelloReply> responseObserver) {
        String name = request.getName();
        HelloReply reply = HelloReply.newBuilder().setGreeting(String.format("Hello %s!", name)).build();

        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }

    @Override
    public void helloAgain(HelloRequest request, StreamObserver<HelloReply> responseObserver) {
        String name = request.getName();
        HelloReply reply = HelloReply.newBuilder().setGreeting(String.format("Hello again %s!", name)).build();

        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }
}
